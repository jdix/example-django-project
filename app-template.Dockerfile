FROM alpine

ENV PYTHONUNBUFFERED 1
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8

EXPOSE ${DJANGO_PORT}

RUN mkdir /django_app
WORKDIR /django_app
COPY . /django_app/

RUN apk add python3 py3-pip \
    && apk add --no-cache --virtual .psycopg2-build-deps python3-dev gcc postgresql-dev musl-dev \
    && pip3 install --ignore-installed distlib pipenv \
    && pipenv lock && pipenv install --system --deploy
    # && apk del --no-cache .psycopg2-build-deps

ENTRYPOINT [ "/django_app/app-entrypoint.sh" ]

CMD [ "gunicorn", "${DJANGO_PROJECT_NAME}.wsgi:application", "--bind", "0.0.0.0:${DJANGO_PORT}" ]
