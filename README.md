# example-django-project

This example project is included as a submodule for gitlab.com/jdix/cdk-django-template. It should be as easy as forking the cdk-django-template project, filling out the config.ini file (see readme there), and swapping this submodule for your code.